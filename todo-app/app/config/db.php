<?php


/*
$servername = "localhost";
$username = "root";
$password = 'Password@312';
$database_name = "tasks";
*/

try {
    $conn = new PDO("mysql:host=mysql;dbname=tasks", 'root', file_get_contents('/run/secrets/mysql/password'));
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
    echo "Connected successfully";
} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

